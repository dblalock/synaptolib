function main()
% run a series of simulations using the digits.dat input set, 
% and print the results to the console.

NUM_RUNS = 3;   % number of networks to construct and avg together
LOAD_DATA = 1;  % whether to load previous data instead of
                    % generating new data

% create an InputSet object to give to our network construction
% and analysis code
inputMat = load('digits.dat')';
inputName = 'digits';
categoryStartIndices = 1:20:400;    % 1, 21, 41, etc
input = InputSet(inputMat, inputName, categoryStartIndices);

% display some statistics about our input set
disp(patternSetStats(inputMat, categoryStartIndices) )

% create a network parameters object so we don't have to pass
% a ton of individual arguments to subsequent functions. Note
% that there are *many* additional parameters that can be set.
% See NetworkParams for more information.
p = NetworkParams();
p.threshold = 1.5;      % set whatever thresh we want
p.log = 1;              % create log file during construction?
p.verbose = 1;          % print to console during construction?


% our simulation command is built to be able to handle an arbitrary
% number of inputs, so it takes a cell of input sets. We therefore
% construct such a cell for our input.
inputSets = cell(1);
inputSets{1} = input;


% actually run the simulation
avgMetrics = simulate(inputSets, p, NUM_RUNS, LOAD_DATA);


% display the results; we only have one input set, so we don't
% actually need the loop, but this demonstrates the appropriate 
% construction to handle an arbitrary number of input sets
for i=1:numel(avgMetrics)
    metrics = avgMetrics{i};
    fprintf('Input Set: %s\n', inputSets{i}.name)
    visualizeNetwork(metrics, inputName);
end

end