function [vect usedIndices] = AddBitErrorsInRange(vect, numBits, rangeStart, rangeStop)
% [vect usedIndices] = AddBitErrorsInRange(vect, numBits, rangeStart, rangeStop)
%
% Given a logical vector, a number of bits to flip, and a range of indices
% eligible to be flipped, returns a bit-error-ed copy of the original
% vector
%
% Also returns the indices that were flipped as a second retval

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

usedIndices = ones(1,numBits) * -1;  % negative so won't match initially
flipIndex = -1;
rangeSize = rangeStop - rangeStart + 1;
if (numBits > rangeSize)
    disp(['ERR: AddBitErrorsInRange: cant flip that many bits: ', num2str(numBits)])
    return
end

for bitNum=1:numBits
    while( any( usedIndices == flipIndex) )
        flipIndex = floor(rand(1)*rangeSize + rangeStart);
    end
    usedIndices(bitNum) = flipIndex;
    vect(flipIndex) = ~vect(flipIndex);
end
    
end