function mat = addBitErrorsToCols(mat, errorsPerCol)
% errMat = addBitErrorsToCols(mat, errorsPerCol)
% 
% Given a logical matrix mat, returns a matrix errMat equal to mat
% except with a certain number of randomly selected values converted to
% their logical negation in each column.

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

    [~, cols] = size(mat);
    for i=1:cols
        mat(:,i) = addBitErrors(mat(:,i), errorsPerCol);
    end

    function [vect errIndices] = addBitErrors(vect, numBits)
    % just calls AddBitErrorsInRange() with the start and end indices
        [vect errIndices] = AddBitErrorsInRange(vect, numBits, 1, length(vect) );
    end

end