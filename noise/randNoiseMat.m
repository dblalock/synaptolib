function mat = randNoiseMat(rows, cols, onesPerCol)
% mat = randNoiseMat(rows, cols, onesPerCol)
%
% Creates a logical matrix of the specified size with exactly the specified
% number of 1 values randomly distributed in each column.
% 
% PARAMETERS:
%   rows - the number of rows in the matrix
%   cols - the number of columns in the matrix
%   onesPerCol - the number of ones to have present in each column
%
% RETURN VALUES:
%   mat - the matrix created
%
% See also RANDNOISEVECT

% just use randNoiseVect() to create each column
mat = zeros(rows, cols);
for i=1:cols
    mat(:,i) = randNoiseVect(rows, onesPerCol);
end

end