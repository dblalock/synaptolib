function vect = addSwapNoise(vect, numErrors)
% errVect = addSwapNoise(vect, numErrors)
%
% Given a vector u, returns a vector v in which numErrors of the nonzero
% elements have been randomly swapped with numErrors of the zero elements
% at random.
% 
% If there are NaNs in the vector, or if the specified number
% of errors is larger than min(num zero elements, num nonzero elements), 
% this function will become angry and crash.

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% -------------------------------
% process input
% -------------------------------

len = length(vect);
numZeros = sum(vect == 0);
numOnes = len - numZeros;

% -------------------------------
% error checking
% -------------------------------

if (any(isnan(vect) ) )
   error('addSwapNoise: vector must not contain NaNs') 
end

if (numErrors > min(numZeros, numOnes) )
   error(['addSwapNoise: number of errors too large: ', num2str(numErrors)]) 
end

% -------------------------------
% swapping
% -------------------------------

% find where all the zero elements and nonzero elements are
zeroIndices = find(vect == 0);
oneIndices = setdiff(1:len, zeroIndices);

% generate random orderings of zeros and ones
zeroPerm = randperm(numZeros);
onePerm = randperm(numOnes);

% take the first few elements of this ordering, and use them
% to determine the indices to swap
zeroSwapIndices = zeroIndices(zeroPerm(1:numErrors));
oneSwapIndices = oneIndices(onePerm(1:numErrors));

% actually swap the indices
vect(zeroSwapIndices) = vect(oneSwapIndices);
vect(oneSwapIndices) = 0;

end