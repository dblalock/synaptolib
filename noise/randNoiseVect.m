function noiseVect = randNoiseVect(len, numOnes)
% noiseVect = randNoiseVect(len, numOnes)
%
% Returns a column vector of length len containing numOnes ones at random
% indices, and 0s at all other indices.
%
% See also RANDNOISEMAT

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% make sure input is sensible
if (len < 1)
   error(['Cannot create vector of length ',num2str(len)]) 
end
if (len < numOnes)
   error(['Cannot create vector of length ',num2str(len), ...
       ' with ',num2str(numOnes),' ones']) 
end

% initialize vect to all 0s
noiseVect = zeros(len, 1);

% select random indices to put 1s in
randIndices = randperm(len);
oneIndices = randIndices(1:numOnes);

% put the ones in
noiseVect(oneIndices) = 1;

end