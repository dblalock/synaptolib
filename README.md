# Overview

SynaptoLib is a MATLAB library for carrying out simulations of 
biologically plausible synaptogenesis networks as described by 
[Levy et al][Lvy].

It's organized as follows:

#### Examples

This directory contains example scripts that process input sets, 
construct networks, and analyze the results using the functions
within the rest of the library. If you'd like to run a simulation
of your own, this is the place to start.

#### Analysis

Within this directory are functions to analyze various network 
characteristics, including mutual information between input and 
output, allocation of output neurons for different input categories,
and numerous other metrics.

The main function is analyzeNetwork(), which returns a struct of 
information created using most of the other functions listed. It 
also generates figures if you set the appropriate flags at the top
of the file. Also worth calling is patternSetStats(), which computes 
various statistics about a set of input or output patterns.

#### Noise

The functions in this directory add noise to patterns in various
forms. In certain cases, they need to make assumptions about the
characteristics of the vectors they're operating on, so be sure
to read the help strings.

#### Visualization

These functions are useful for creating figures specific to neural
networks. However, the easiest way to create visualizations of your
network is by setting the appropriate flags in analyzeNetwork().


#### Util

These functions are handy and used to support the rest of the library.
Several are taken from the Mathworks file exchange--you can check the 
source for original authorship and licensing.


[Lvy]: http://faculty.virginia.edu/levylab/
