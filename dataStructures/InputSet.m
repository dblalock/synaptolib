classdef InputSet
% A data structure for the inputs to our neural networks
%
% PROPERTIES:
%   -inputPatterns
%       A matrix whose columns are the individual patterns on which
%       to train the network
%   -name
%       A name for the input to display on figures
%   -catStartIdxs
%       A vector denoting the column of the input at which each 
%       pattern category begins; ie, if there are 10 patterns, with
%       the first 4 in one category and the next 6 in another, this 
%       vector would need to be [1 5]. Note that patterns of the same
%       category MUST be grouped together.
%
% METHODS:
%   None

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

   % can't change an inputset once it's created
   properties(GetAccess='public', SetAccess='private')
       inputPatterns;
       name;
       catStartIdxs;
   end
   
   methods
       function obj = InputSet(inputPatterns, name, catStartIdxs)
          obj.inputPatterns = inputPatterns;
          obj.name = name;
          obj.catStartIdxs = catStartIdxs;
       end
   end
    
end