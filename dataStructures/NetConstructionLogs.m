classdef NetConstructionLogs
% This class serves as a simple data structure containing
% individual logs for various attributes of a network as it's
% being constructed. All logs are cells, since the number of
% entries that will be made is unknown when construction begins.
%
% PROPERTIES
%   -activation_log = cell(1);
%       Average activation across neurons after each round of 
%       synaptogenesis/synaptic shedding
%   -synaptogen_log = cell(1);
%       Number of synapses added in each round of synaptogenesis/ 
%       synaptic shedding
%   -synaptoshed_log = cell(1);
%       Number of synapses shed in each round of synaptogenesis/
%       synaptic shedding
%   -synapses_log = cell(1);
%       Total number of synapses in the network after each round of
%       synaptogenesis/synaptic shedding
%   -synapses_individual_log = cell(1);
%       Number of synapses of each neuron in the network after each
%       round of synaptogenesis/synaptic shedding
%   -receps_individual_log = cell(1);
%       Receptivity of each neuron in the network after each
%       round of synaptogenesis/synaptic shedding

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

    properties
        activation_log = cell(1);
        synaptogen_log = cell(1);
        synaptoshed_log = cell(1);
        synapses_log = cell(1);
        synapses_individual_log = cell(1);
        receps_individual_log = cell(1);
    end
    
    % default constructor
    methods
        function obj = NetConstructionLogs()
           obj;
        end
    end
end