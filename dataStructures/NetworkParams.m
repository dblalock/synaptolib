classdef NetworkParams
% Data structure defining possible parameters that can be set for network
% construction. Not a struct so that the fields can be fixed ahead of time 
% and defined in their own file.
%
% PROPERTIES:
%   Basic Network Properties
%   -neuronCount = 40;
%       The number of output neurons in the network 
%   -threshold = 1;
%       The level of activation a neuron must receive to fire
%
%   Overall Network Construction
%   -interSynaptogenIterations = 10;
%       The number of times the network should be trained on the
%       input set in between the creation and shedding of synapses
%   -synaptogenKickstartRounds = 0;
%       The number of rounds synaptogenesis should be carried out
%       to provide random "starter" connections before normal 
%       network construction begins.
% 
%   Receptivity and Avidity
%   -minFiringRate = .08;
%       The firing rate all neurons must achieve for network
%       construction to terminate. Also the value at which
%       receptivity drops from its default (max) level to ~0.
%   -activityRateConst = .01;
%       The rate constant used to compute the moving average level
%       of activity for each neuron. This is used for determining
%       whether neurons have acheived their minimum firing rate.
%   -maxReceptivity = .01;
%       The receptivity value of a neuron firing below its minimum
%       firing rate. This should be a small number so that only a
%       few new synapes are created in a given round of synaptogenesis
%   -avidityConst = .5;
%       The constant k used in the expression a(i) = k / (k + sum(Wij) )
%
%   Synaptonesis/Synaptic Shedding
%   -wNewConn = .4;
%       The weight assigned to a newly created synapse. This should
%       be high enough that isn't immediately shed, but low enough
%       that it isn't given disproportionate importance.
%   -wRateConst = .01;
%       The rate constant governing how quickly synaptic weights 
%       can change
%   -wCutoff = .025;
%       All synapses with weights below this threshold are removed
%       during synaptic shedding.
% 
%   Termination of Network Construction
%   -ensureStability = 1;
%       By default, network construction terminates when the moving
%       average firing rate of all neurons is above the minimum
%       firing rate. When this is set, however, the additional
%       restriction that the average change in the number of synapses
%       between rounds must be below synapseChangeCutoff.
%   -synapsesChangeRateConst = .01;
%       The rate constant used to compute the above (moving) average
%   -synapsesChangeCutoff = .01;
%       If ensureStability is set, the average change in synapse count
%       between rounds for the whole network must fall below this value
%       for network construction to terminate.
%   -synaptogenRoundsLimit = 5000;
%       If a network isn't stable after this many rounds of 
%       synaptogenesis/synaptic shedding, network construction is
%       terminated anyway.
%
%   Miscellaneous
%   -log = 1;
%       If set, a log file will be created and information about the
%       network will be logged during construction.
%   -verbose = 0;
%       If set, information about the network will be printed to the 
%       console during network construction.
%
% METHODS:
%   -logSelf(fileId)
%       Writes a textual form of this object to the specified file

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


    properties
        % basic network properties
        neuronCount = 40;
        threshold = 1;
        
        % overall network construction
        interSynaptogenIterations = 10;
        synaptogenKickstartRounds = 0;
        
        % receptivity / avidity properties
        minFiringRate = .08;
        activityRateConst = .01;
        maxReceptivity = .01;
        avidityConst = .5;
        
        % synaptonesis / synaptic shedding properties
        wNewConn = .4;
        wRateConst = .01;
        wCutoff = .025;
        
        % termination of network construction
        ensureStability = 1;
        synapsesChangeRateConst = .01;
        synapsesChangeCutoff = .01;
        synaptogenRoundsLimit = 5000;
        
        % miscellaneous
        log = 1;
        verbose = 0;
    end
    
    % default constructor
    methods
        function obj = NetworkParams()
           obj;
        end
        
        function logSelf(obj,fileId)
            
            % there *is* a cleaner way to do this, but I don't feel
            % like figuring out the code to loop through each property
            % and recursively print everything at the moment
            fprintf(fileId, 'neuronCount: %d\n', obj.neuronCount);
            fprintf(fileId, 'minFirinRate: %.3f\n', obj.minFiringRate);
            fprintf(fileId, 'threshold: %.3f\n', obj.threshold);
            fprintf(fileId, 'interSynaptogenIterations: %d\n', obj.interSynaptogenIterations);
            fprintf(fileId, 'synaptogenKickstartRounds: %d\n', obj.synaptogenKickstartRounds);
            fprintf(fileId, 'threshold: %.3f\n', obj.threshold);
            fprintf(fileId, 'activityRateConst: %.3f\n', obj.activityRateConst);
            fprintf(fileId, 'maxReceptivity: %.3f\n', obj.maxReceptivity);
            fprintf(fileId, 'wNewConn: %.3f\n', obj.wNewConn);
            fprintf(fileId, 'wRateConst: %.3f\n', obj.wRateConst);
            fprintf(fileId, 'wCutoff: %.3f\n', obj.wCutoff);
            fprintf(fileId, 'ensureStability: %d\n', obj.ensureStability);
            fprintf(fileId, 'synapsesChangeRateConst: %.3f\n', obj.synapsesChangeRateConst);
            fprintf(fileId, 'synapsesChangeCutoff: %.3f\n', obj.synapsesChangeCutoff);
            fprintf(fileId, 'synaptogenRoundsLimit: %d\n', obj.synaptogenRoundsLimit);
            fprintf(fileId, 'log: %d\n', obj.log);
            fprintf(fileId, 'verbose: %d\n', obj.verbose);
        end
    end
    
end