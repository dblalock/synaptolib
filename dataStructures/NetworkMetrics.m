classdef NetworkMetrics
% Data structure defining metrics calculated for a finished network.
%
% PROPERTIES:
%   Overall network properties
%   -totalNeurons =  0
%       The number of output neurons in the network
%   -nonzeroNeurons = 0
%       The number of output neurons in the network that fire
%       to at least one input pattern
%   -weights = []
%       The synaptic weights across all the neurons in the network that
%       fire
%   -synapseCounts = []
%       The total number of synaspes for each nonzero neuron
%       in the network
%   -weightSums = []
%       The sum of the weights of all synapes for each nonzero
%       neuron in the network
%   -catStartIdxs = []
%       The first indices within the input set at which patterns of 
%       a given category occur (patterns within the same category are 
%       assumed to be grouped together)
%   -eigenVals = []
%       The dominant eigenvalues of the covariance matrix for the features
%       associated with each category
%   -netConstructionLogs = []
%       The NetConstructionLogs object associated with this network. Note
%       that this will be null if the network was constructed without
%       logging enabled.
% 
%   Network output across input set
%   -numInputPatterns = 0
%       The number of patterns in the input set
%   -excitation = []
%       The PATTERN_COUNT x NEURON_COUNT excitation matrix 
%       produced when the the input set is multiplied by the
%       weight matrix
%   -output = []
%       The set of indices in the excitation matrix greater
%       than the firing threshold
%   -inputSetStats = []
%       The struct returned by patternSetStats() when called on the 
%       input patterns
%   -outputSetStats = []
%       The struct returned by patternSetStats() when called on the 
%       output patterns
%   -activs = []
%       The average activity level (firing rate) of each nonzero
%       output neuron
%   -minAct = 0
%       The minimum activity level (firing rate) of any nonzero
%       output neuron
%   -maxAct = 0
%       The maximum activity level (firing rate) of any nonzero
%       output neuron
%   -stdAct = 0
%       The standard deviation of activity level across
%       nonzero output neurons
%   -meanAct = 0
%       The average activity level across all nonzero ouptut
%       neurons
% 
%   Costs
%   -synapseCosts = 0
%       The combined energy cost associated with the synapses of
%       all nonzero output neurons across the whole input set
%   -firingCosts = 0
%       The combined energy cost associated with the firing of
%       all nonzero output neurons across the whole input set
%   -meanCost = 0
%       The sum of the synapse costs and the firing costs, divided
%       by the number of input patterns
%   -meanCostPerNeuron = 0
%       The mean cost divided by the number of nonzero output neurons
%   -codeWordCounts = 0
%       The number of distinct codewords produced by all nonzero
%       output neurons across the whole input set
% 
%   Information and statistical dependence measures
%   -statDep = 0
%       The statistical dependence of the output matrix
%   -outputInfo = 0
%       The information conveyed by the output codewords for
%       the entire input set
%   -mutualInfo = 0
%       The mutual information between the output codewords and
%       the input patterns 
%   -bitsPerJ = 0
%       The mutual information divided by the aforementioned mean 
%       energy cost
% 
%   Resource allocation
%   -neuronAllocs = []
%       The number of nonzero output neurons allocated to represent
%       each category. These are the sums of the probabilities that 
%       neuron j will fire for a given category for all j
%   -repCapacities = []
%       The information of the codewords produced in reponse to all
%       patterns of each category
%   -repInfos = []
%       The mutual information between the input patterns of each category
%       and their corresponding output patterns
%   -synapsesForCategories = []
%       The average number of synapses for all neurons that ever fire
%       in reponse to the patterns of each category
%   -weightSumsForCategories = []
%       The average sum of the synaptic weights for all neurons that 
%       ever fire in reponse to the patterns of each category  
% 
%   Classification and errors
%   -numClassifyErrs = 0
%       The number of output codewords that are closer in Euclidean
%       space to the centroid of the codewords for a different category
%   -discardFractions = []
%       When computing errors resulting from discarding (nonzero) 
%       output neurons, this is the vector of fractions discarded
%       to compute the various error rates
%   -discardErrRates = []
%       For a given fraction of discarded output neurons, the percentage 
%       of the time in which there is at least one category for which
%       none of the output neurons code. I.e., the probability that it is
%       no longer possible to identify at least one of the categories.
%   -classificationInfo = 0
%       The mutual information between the input classifications and 
%       the output classifications
%   -classificationBitsPerJ = 0
%       The classificationInfo divided by the mean energy cost
%
% METHODS:
%   None
%
% See also ANALYZENETWORK, PATTERNSETSTATS

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


    properties
        % overall network properties
        totalNeurons =  0;
        nonzeroNeurons = 0;
        weights = [];
        synapseCounts = [];
        weightSums = [];
        catStartIdxs = [];
        eigenVals = [];
        netConstructionLogs = [];
        
        % network output across input set
        numInputPatterns = 0;
        excitation = [];
        output = [];
        inputSetStats = [];
        outputSetStats = []
        activs = [];
        minAct = 0;
        maxAct = 0;
        stdAct = 0;
        meanAct = 0;
        
        % costs
        synapseCosts = 0;
        firingCosts = 0;
        meanCost = 0;
        meanCostPerNeuron = 0;
        codeWordCounts = 0;
        
        % information and statistical dependence measures
        statDep = 0;
        outputInfo = 0;
        mutualInfo = 0;
        bitsPerJ = 0;
        
        % resource allocation
        neuronAllocs = [];
        repCapacities = [];
        repInfos = [];
        synapsesForCategories = [];
        weightSumsForCategories = [];
        
        % classification and errors
        numClassifyErrs = 0;
        discardErrRates = [];
        discardFractions = [];
        classificationInfo = 0;
        classificationBitsPerJ = 0;
    end
    
    % default constructor
    methods
        function obj = NetworkMetrics()
           obj;
        end
    end
    
end
