function cosineMat = cosineComp(A, B)
% performs a cosine comparison between each column of A and each column of
% B, returning the results in a cols(A) x cols(B) matrix

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013 Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% let's save some typing...
function mags = columnMagnitudes(M)
  mags = sqrt( sum( M.*M) );
end

% sparse and logical matrices break if we don't do this
A = full(double(A));
B = full(double(B));

% compute |u| and |v|
Amags = columnMagnitudes(A);
Bmags = columnMagnitudes(B);
magMat = Amags' * Bmags;

% compute |u . v|
dotProds = A' * B;

% compute |u . v| / (|u| * |v|)
cosineMat = dotProds ./ magMat;

end