function symbols = cols2symbols(A)
% symbols = cols2symbols(A)
%
% Converts unique columns to unique symbols. I.e., if each column
% is a codeword, then these codewords are numbered and the returned
% row vector is the number assigned to each column.
%
% Example:
%
% >> x = [4 4 2 1 1 4];
% >> cols2symbols(x)
% 
% ans =
% 
%      1     1     2     3     3     1

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013 Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

[~,~,symbols] = unique(A', 'rows', 'stable');
symbols = symbols';

end