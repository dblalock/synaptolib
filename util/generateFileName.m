function name = generateFileName(inputName, p, runNumber)
% name = generateFileName(inputName, p, runNumber)
%
% Creates a file name (without the extension) for a given
% network input name, set of network parameters, and run
% number. This allows us an easy way to save and load files
% for different networks without constantly calling sprintf()
% and worrying about consistent naming conventions, etc.
%
% PARAMETERS:
%   inputName - an arbitrary (but hopefully informative) string
%       serving as a name for the network's input matrix
%   p - a networkParams object describing how the network is to
%       be / was constructed
%   runNumber - an optional argument describing which of n
%       networks in a series this is. Ie, if multiple networks
%       are being constructed in the same manner and averaged
%       for analysis, this argument allows one to distinguish
%       their respective data files. If runNumber is set to -1,
%       it will not be included in the returned file name.
%
% RETURN VALUES:
%   name - a string of the form:
%           %s_fire%d_thresh%d[_%d]
%       where the values are specified by: 
%           inputName,
%           p.minFiringRate*1000,
%           p.threshold*1000,
%           [runNumber]
%       Note that the name does not include an extension. This
%       is so that log files, data files, etc, can all have their
%       their own appended as appropriate.

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

SetDefaultValue(3, 'runNumber', -1);

if (runNumber ~= -1)
    name = sprintf('%s_fire%d_thresh%d_%d', ...
        inputName, p.minFiringRate*1000,p.threshold*1000, runNumber);
else
    name = sprintf('%s_fire%d_thresh%d', ...
        inputName, p.minFiringRate*1000,p.threshold*1000);
end

end