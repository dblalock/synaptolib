function relFreqs = calcRelativeFrequencies(catStartIdxs, numPatterns)
% relFreqs = calcRelativeFrequencies(catStartIdxs, numPatterns)
%
% Calculate the relative frequency of each category given the indices
% within the input set at which the categories each start and the total
% number of patterns in the input set.
%
% PARAMETERS:
%   catStartIdxs - a vector of the indices within the input set at which
%                  at which each category starts
%   numPatterns - the total number of patterns within the input set
%
% RETURN VALUES:
%   relFreqs - a column vector of the relative frequencies of each
%   category

stopIdxs = stopIndicesFromStartIndices(catStartIdxs, numPatterns);
patternsInCats = stopIdxs - catStartIdxs + 1;
relFreqs = (patternsInCats / numPatterns)';

end