function S2 = scaleStruct(S, k)
% S2 = scaleStruct(S)
%
% Given a struct S and a constant k, returns a struct S2 such that
% S2.<fieldName> = S.<fieldName> * k for all fieldNames whose 
% corresponding values are numeric.
%
% For values that are not numeric, the value from S1 is copied to S.

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

fNames = fieldnames(S);
numFields = length(fNames);

for i=1:numFields
    
    % get value for each current field
    fNameCell = fNames(i);
    fName = fNameCell{:};
    val = S.(fName);
    
    % if field numeric, multiply by k
    if (isnumeric(val) )
        S2.(fName) = val * k;
    
    % if field non-numeric, use value from first struct
    else
        S2.(fName) = val;
    end
end

end