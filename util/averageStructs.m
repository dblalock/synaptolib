function S = averageStructs(varargin)
% S = averageStructs(varargin)
%
% Given n structs S1...Sn, returns a struct S such that
% S.<fieldName> = mean(S1.<fieldName>...Sn.<fieldName>) for all 
% fieldNames whose corresponding values are both numeric
% and the same length in both Si and Sj for all i ~= j.
%
% Actually, it gets slightly strange for field names that are 
% numeric but aren't the same length; the length of the field 
% in the *first* struct is assumed to be correct, and the final 
% value is the sum of the fields in all structs where this length
% is matched, divided by the total number of structs. Effectively,
% it's probably garbage if the lengths vary.
%
% If the values are not numeric or not the same length, the
% value from S1 is copied to S.
%
% This function will throw an error if the field names in
% any two structs are not the same.
%
% Example:
%
% >> s1
%
% s1 = 
%       a: 1
%       b: 2
%       c: [1 2 3]
%     foo: 'bar'
% 
% >> s2
%
% s2 = 
%       a: 1
%       b: 4
%       c: 3
%     foo: 'baz'
% 
% >> s3
%
% s3 = 
%       a: 0 - 2.0000i
%       b: 2
%       c: 5
%     foo: 'bar'
% 
% >> averageStructs(s1, s2, s3)
% 
% ans = 
%       a: 0.6667 - 0.6667i
%       b: 2.6667
%       c: [0.3333 0.6667 1]
%     foo: 'bar'

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


numStructs = nargin;

if (numStructs < 1)
    S = [];
    return
elseif (numStructs == 1)
    S = varargin{1};
    return
end

S = varargin{1};
for i=2:(numStructs)
    S = addStructs(S, varargin{i});
end
S = scaleStruct(S, 1/numStructs);

end