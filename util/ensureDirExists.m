function ensureDirExists(dirName)
% ensureDirExists(dirName)
%
% checks if the specified directory exists and, if not, creates it

if ~exist(dirName, 'dir')
   mkdir(dirName); 
end

end