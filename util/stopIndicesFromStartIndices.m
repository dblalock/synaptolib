function stopIndices = stopIndicesFromStartIndices(startIndices, maxIndex)
% stopIndices = stopIndicesFromStartIndices(startIndices, maxIndex)
%
% Given a vector of indices in which categories start, returns a vector of
% the indices in which they stop
%
% i.e., [1 5 7] with a max index of 10 returns [4 6 10]

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

indexCount = length(startIndices);
stopIndices = zeros(1, indexCount);
for i=1:(indexCount-1)
    stopIndices(i) = startIndices(i+1) - 1;
end
stopIndices(end) = maxIndex;

end