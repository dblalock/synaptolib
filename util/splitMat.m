function C = splitMat(A, sectionStartIndices)
% patternsCell = patternsForCategories(patternsMat, catStartIndices)
% 
% Given a matrix A and the initial indices of the sections into which 
% it should be split, returns a cell whose entries are the columns of
% A in each section.
%
% Examples:
%
% M = magic(5)
% 
% M =
% 
%     17    24     1     8    15
%     23     5     7    14    16
%      4     6    13    20    22
%     10    12    19    21     3
%     11    18    25     2     9
% 
% >> C = splitMat(M, [1 3])
% 
% C = 
% 
%     [5x2 double]
%     [5x3 double]
% 
% >> C{1}
% 
% ans =
% 
%     17    24
%     23     5
%      4     6
%     10    12
%     11    18
% 
% >> C{2}
% 
% ans =
% 
%      1     8    15
%      7    14    16
%     13    20    22
%     19    21     3
%     25     2     9

numSections = length(sectionStartIndices);
C = cell(numSections,1);

[~, numCols] = size(A);
sectionStopIndices = stopIndicesFromStartIndices(sectionStartIndices, numCols);

for i=1:numSections
    startIdx = sectionStartIndices(i);
    stopIdx = sectionStopIndices(i);
    C{i} = A(:, startIdx:stopIdx);
end

end