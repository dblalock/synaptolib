function B = repColVectFast(vect, reps)
% B = repColVectFast(vect, reps)
%
% Returns a matrix composed of reps columns each equal to vect. This
% implementation is significantly faster than using repmat(), and
% so its use is strongly encouraged in network simulations.

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

B = vect(:, ones(reps, 1));

end