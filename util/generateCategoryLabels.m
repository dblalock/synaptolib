function labels = generateCategoryLabels(len, catStartIndices)
% labels = generateCategoryLabels(catStartIndices)
%
% Generates a vector of integer labels for a group of categories.
%
% More precisely, for a set of categories [1..k], generates a vector with 
% elements in the range [1..k] in ascending order, with the transitions 
% between values taking place at each of the indices specified.
%
% PARAMETERS:
%   len - the length of the label vector, equal to the total number of
%       patterns within all categories
%   catStartIndices - the first indices within the input set at which
%       patterns of a given category occur (patterns within the same
%       category are assumed to be grouped together)
%
% RETURN VALUES:
%   labels - a vector of length len whose values are equal to i for
%       i = [catStartIndices(i), catStartIndices(i+1) ), or 
%       [catStartIndices(i), length] for the final category.

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

numCategs = length(catStartIndices);
labels = zeros(len, 1);
for i=1:numCategs
    startIdx = catStartIndices(i);
    labels(startIdx:end) = labels(startIdx:end) + 1;
end

end