function S = addStructs(S1, S2)
% S = addStructs(S1, S2)
%
% Given two structs S1 and S2, returns a struct S such that
% S.<fieldName> = S1.<fieldName> + S2.<fieldName> for all 
% fieldNames whose corresponding values are both numeric
% and the same length in both S1 and S2.
%
% If the values are not numeric or not the same length, the
% value from S1 is copied to S.
%
% This function will throw an error if the field names in
% S1 and S2 are not the same.
%
% Example:
%
% >> s1
% 
% s1 = 
% 
%       a: 1
%       b: 2
%       c: [1 2 3]
%     foo: 'bar'
% 
% >> s2
% 
% s2 = 
% 
%       a: 1
%       b: 4
%       c: 3
%     foo: 'baz'
% 
% >> addStructs(s1, s2)
% 
% ans = 
% 
%       a: 2
%       b: 6
%       c: [1 2 3]
%     foo: 'bar'

% No license for this file since I put it up on FileExchange

fNames1 = fieldnames(S1);
fNames2 = fieldnames(S2);

diff = setdiff(fNames1, fNames2);
if ~isempty(diff)
   error('addStructs: structures do not contain same field names')
end

numFields = length(fNames1);

for i=1:numFields
    
    % get values for each struct for this field
    fNameCell = fNames1(i);
    fName = fNameCell{:};
    val1 = S1.(fName);
    val2 = S2.(fName);
    
    % if field non-numeric, use value from first struct
    if (~isnumeric(val1) || ~isnumeric(val2) )
        S.(fName) = val1;
        
    % if fields numeric but not the same length, use value
    % from first struct
    elseif (any(size(val1) ~= size(val2)) )
        S.(fName) = val1;
        
    % if fields numeric and same length, add them together
    else
        S.(fName) = val1 + val2;
    end
end

end