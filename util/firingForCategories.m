function firingMatrix = firingForCategories( output, categStartIndices)
% firingMatrix = firingForCategories( output, categStartIndices)
%
% Given an output matrix and indices of the rows in which the output
% for each category starts, returns a matrix containing the number of times
% each neuron fires for each category. This matrix is of dimensions 
% NUMBER_OF_CATEGORIES x NUMBER_OF_NEURONS.
%
% Output is assumed to have a column for each neuron, 
% and a row for each input pattern

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013 Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


cat_count = length(categStartIndices);
[outputRows, outputCols] = size(output);

% we need to extract the firing for each category within the output matrix;
% to do this, we need the start and stop row indices for each category; we 
% were given the start indices as a parameter, so we just need to calculate
% the stop indices
categStopIndices = stopIndicesFromStartIndices(categStartIndices, outputRows);

% now that we know where categories start and stop, we can sum up how much
% each neuron fires in response to each category
firingMatrix = zeros(cat_count, outputCols);
for catNum=1:cat_count
   rowStartIdx = categStartIndices(catNum);
   rowStopIdx = categStopIndices(catNum);
   rowRange = rowStartIdx:rowStopIdx;
   firing = sum(output(rowRange, :) );
   firingMatrix(catNum, :) = firing;
end

end
