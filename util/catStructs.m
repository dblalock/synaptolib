function S = catStructs(varargin)
% S = catStructs(varargin)
%
% Given n structs S1...Sn, returns a struct S such that
% S.<fieldName> = [S1.<fieldName>(:); ...; S2.<fieldName>(:)] for all 
% fieldNames.
%
% This function will throw an error if the field names in
% S1 and S2 are not the same.

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


function s = catTwoStructs(S1,S2)
    fNames1 = fieldnames(S1);
    fNames2 = fieldnames(S2);

    diff = setdiff(fNames1, fNames2);
    if ~isempty(diff)
       error('addStructs: structures do not contain same field names')
    end

    numFields = length(fNames1);

    for i=1:numFields

        % get values for each struct for this field
        fNameCell = fNames1(i);
        fName = fNameCell{:};
        val1 = S1.(fName);
        val2 = S2.(fName);

        s.(fName) = [val1(:); val2(:)];
    end
end

numStructs = nargin;

if (numStructs < 1)
    S = [];
    return
elseif (numStructs == 1)
    S = varargin{1};
    return
end

S = varargin{1};
for j=2:(numStructs)
    S = catTwoStructs(S, varargin{j});
end


end