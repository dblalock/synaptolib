function dists = euclideanDists(A,B)
% dists = euclideanDists(A,B)
%
% Returns a matrix containing the euclidean distances
% between each column of A and each column of B. Rows
% are values for each of A's columns and columns are
% values for each of B's columns.

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013 Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.


[rowsA, colsA] = size(A);
[rowsB, colsB] = size(B);

% ensure columns are in same dimensional space
if (rowsA ~= rowsB)
   error(['euclideanDists(): A contians ',num2str(rowsA),...
       ' rows, but B contains ',num2str(rowsB)]) 
end

dists = zeros(colsA, colsB);

for i=1:colsA
    aCol = A(:,i);
    aColMat = repColVectFast(aCol,colsB);
    distances = B - aColMat;                % find L1 distances
    distances = distances .* distances;     % square them
    dists(i,:) = sqrt(sum(distances, 1) );  % sqrts of sums of squares
end

end