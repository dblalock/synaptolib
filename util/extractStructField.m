function vals = extractStructField(StructsCell, fieldName)
% vals = extractStructField(StructsCell, propName)
%
% Extracts the value of propName for each struct (or class) provided.
%
% PARAMETERS:
%   StructsCell - a cell whose entries are a sequence of structs
%   propName - the name of the field to be extracted from each struct
%
% RETURN VALUES:
%   vals - a matrix containing the values of the field for each of 
%       the supplied structs. Note that vals will have the same shape 
%       as StructsCell
%
% Note that this function will probably crash if the extracted value
% isn't a scalar, since the resulting matrix won't have sensible
% dimensionality

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

numStructs = numel(StructsCell);
vals = zeros(numStructs, 1);
for i=1:numStructs
    s = StructsCell{i};
    vals(i) = s.(fieldName);
end

% returned values should have same shape as structs cell
[r, c] = size(StructsCell);
vals = reshape(vals, r, c);

end