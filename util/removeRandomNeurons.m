function [W2 keepIdxs discardIdxs] = removeRandomNeurons(W, k)
% W2 = removeRandomNeurons(W, k)
%
% Removes a portion k of the neurons in weight matrix W
% 
% PARAMETERS:
%   W - a weight matrix whose rows are the synaptic weights to each neuron
%       for individual patterns and whose columns are (consequently)
%       the synaptic weights to each pattern for individual neurons
%   k - the portion of neurons to remove; e.g., .75 to remove three
%           quarters of them.
%
% RETURN VALUES:
%   W2 - the original weight matrix with a number of columns removed
%       equal to floor( k * number of columns in W )
%   keepIdxs - the indices of the neurons kept from W
%   discardIdxs - the indices of the neurons discarded from W

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

[~, currentNeuronCount] = size(W);

% generate a random set of indices
randIndxs = randperm(currentNeuronCount);

% determine how many neurons to keep
maxIdxToKeep = ceil( (1-k) * currentNeuronCount);

% determine which neurons to keep and which to discard
keepIdxs = randIndxs(1:maxIdxToKeep);
discardIdxs = randIndxs( (maxIdxToKeep+1):end );

% copy the selected neurons to W2
W2 = W(:, keepIdxs);

end