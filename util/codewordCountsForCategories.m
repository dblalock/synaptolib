function counts = codewordCountsForCategories(codewords, catStartIndices)
% counts = codewordCountsForCategories(codewords)
%
% Given a matrix whose columns are codewords and the indices, at
% which each category starts, returns the number of distinct patterns
% for each category
% 
% PARAMETERS:
%   codewords - a matrix whose columns represent codewords.
%   catStartIndices - the first indices within the codeword matrix at
%       which patterns of a given category occur (patterns within the
%       same category are assumed to be grouped together)
%
% RETURN VALUES:
%   counts - a row vector containing the number of unique patterns
%       within each category
%
% Example:
% >> M = [eye(2) ones(2)]
% 
% M =
% 
%      1     0     1     1
%      0     1     1     1
% 
% >> codewordCountsForCategories(M, [1 3])
% 
% ans =
% 
%      2     1
% 
% Explanation: the first two columns are one category, and the second
% two columns are another category. The first category has two distinct
% codewords ( [1; 0] and [0; 1]), while the second only has one ([1; 1]).

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

catPatterns = splitMat(codewords, catStartIndices);
numCats = length(catPatterns);

counts = zeros(1, numCats);
for i=1:numCats
    patterns = catPatterns{i};
    symbols = cols2symbols(patterns);
    counts(i) = length(unique(symbols));
end

end