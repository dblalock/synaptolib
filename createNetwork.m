function [W L] = createNetwork(InputObj, P) 
% [W L] = createNetwork(InputObj, P)
%
% Creates a biologically-plausible, single-layer, feed-forward neural network
% using the given input and network parameters.
%
% PARAMETERS:
%   InputObj - an InputSet object containing the patterns on which to train
%       the network
%   P - a NetworkParams object describing the various parameters of the
%       network, such as number of output neurons, threshold, any many
%       others. When not specified, the default networkParams() object is
%       used.
%
% RETURN VALUES:
%   W - a sparse weight matrix describing the set of all synapses in the
%       network. Each input feature has a row, and each output neuron has
%       a column.
%   L - a NetConstructionLogs object recording various attributes of
%       the network as it was being created. This is created only if p
%       has the 'log' property set to 1

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

SetDefaultValue(2, 'P', NetworkParams() );

% ================================================================
% CONSTANTS
% ================================================================

% -------------------------------
% network structure
% -------------------------------

NEURON_COUNT = P.neuronCount;

% -------------------------------
% constants pertaining to code execution
% -------------------------------

ENSURE_STABILITY = 1;   % Uses a more rigorous stability metric
VERBOSE = P.verbose;  % For debugging; every 10 rounds of synaptogen, prints stuff
LOG = P.log;

% -------------------------------
% input data
% -------------------------------

input = InputObj.inputPatterns;
inputName = InputObj.name;

[FEATURES_COUNT PATTERNS_COUNT] = size(input);

% create input matrices that get used for synaptic modification
% ahead of time so we don't have to do it in innermost loop
inputPatternMats = cell(PATTERNS_COUNT, 1);
for i=1:PATTERNS_COUNT
    inputPattern = input(:,i);
    inputPatternMats{i} = repColVectFast(inputPattern, NEURON_COUNT);
end

% expected value of input for our dW rule
E_X = mean(input, 2);
E_X = repColVectFast(E_X, NEURON_COUNT);

% -------------------------------
% constants pertaining to synaptogenesis/shedding
% -------------------------------

% scale factor to prevent too many connections early on
RECEPTIVITY_SCALE_FACTOR = P.maxReceptivity;   

% constants for average firing rate
ACTIVITY_RATE_CONST = P.activityRateConst;

% minimum firing rate neurons seek to achieve
MIN_FIRING_RATE = P.minFiringRate;

% constant used for avidity equation
AVIDITY_K = P.avidityConst;

% iterations through input set between rounds of synaptogenesis
INTER_SYNAPTOGEN_ITERATIONS = P.interSynaptogenIterations;

% number of rounds of synaptogenesis to create initial random network
SYNAPTOGEN_KICKSTART_ROUNDS = P.synaptogenKickstartRounds;

% -------------------------------
% constants pertaining to threshold and weight modification
% -------------------------------
% weight of newly formed connection
W_NEW_CONN = P.wNewConn;

% rate constant for weight modification
W_RATE_CONST = P.wRateConst;

% cutoff below which synapses are shed
W_CUTOFF = P.wCutoff;

% minimum excitation for neurons to fire
THRESH = P.threshold;

% -------------------------------
% constants defining termination of network construction
% -------------------------------

% if ENSURE_STABILITY is set, the average change in synapses
% between rounds must be below this threshold for the network
% to be considered stable
DELTA_SYNAPSES_FINISH_THRESH = P.synapsesChangeCutoff;

% rate constant for the above moving average
DELTA_SYNAPSES_RATE_CONST = P.synapsesChangeRateConst;

% if network not stable after this many rounds, cut off
% construction anyway
SYNAPTOGEN_ROUNDS_LIMIT = P.synaptogenRoundsLimit;

% ================================================================
% INTIALIZATION
% ================================================================

% -------------------------------
% initialize logs
% -------------------------------

% print out the network parameters if desired
if (VERBOSE)
   disp(P)
end

if (LOG)
    % initialize our logs of data that we return
    L = NetConstructionLogs;
    
    % open up a text file in which to write output, if desired
    fileName = generateFileName(inputName, P);
    logFile = fopen([fileName,'_log.txt'], 'w');
    if (logFile == -1) 
        error(['failed to open log_file: ', fileName]); 
    end

    % print the network parameters at the top of the log file
    P.logSelf(logFile)
else
    L = [];
end

% define a function we can call to do output that will pipe
% it to the appropriate streams automatically, so we don't
% have to keep checking flags in the rest of the code
function log(varargin)
    if (VERBOSE)
        fprintf(varargin{:});
    end
    if (LOG)
        fprintf(logFile, varargin{:});
    end
end

% -------------------------------
% initialize weights
% -------------------------------

% initialize weight matrix to zeros; since we don't anticipate
% that many connections, make it a sparse matrix
%round(NEURON_COUNT * thresh * 10);
estimatedConnections = 13;  % empirically derived
W = spalloc(FEATURES_COUNT, NEURON_COUNT, estimatedConnections);

% -------------------------------
% initialize stability measures
% -------------------------------

minRoundsOfStability = 100;     %TODO put in NetworkParams
stableRounds = 0;

% -------------------------------
% pre-allocate space
% -------------------------------

excitation = zeros(1, NEURON_COUNT);
Y = zeros(FEATURES_COUNT, NEURON_COUNT);
Z = zeros(1, NEURON_COUNT);
dW = zeros(FEATURES_COUNT, NEURON_COUNT);

% ================================================================
% MAIN LOOP
% ================================================================
tic;

% -------------------------------
% network construction
% -------------------------------
synaptogen_round = 0;
networkFinished = 0;
delta_synapses = 3;     % has to be > 0 so won't terminate prematurely
old_synapses_count = 0;
avgActivation = zeros(1, NEURON_COUNT);
while(~networkFinished)
    
    % -------------------------------
    % synaptogenesis
    % -------------------------------
    
    synaptogen_round = synaptogen_round + 1;
    
    % rectangular receptivity rule
    recep_vect = RECEPTIVITY_SCALE_FACTOR * (avgActivation < MIN_FIRING_RATE);
    weightsForInputs = sum(W, 2);
    avidity_vect = (AVIDITY_K ./ (AVIDITY_K + weightsForInputs ));
    recep_matrix = avidity_vect * recep_vect;
%     recep_matrix = repRowVectFast(recep_vect, FEATURES_COUNT);
    
    new_connections = rand(FEATURES_COUNT, NEURON_COUNT) < recep_matrix;
    new_connections = sparse(new_connections);  % had BETTER be sparse...
    new_connections = new_connections - and(new_connections, W);  % don't add duplicates
    new_connections = new_connections .* W_NEW_CONN;
    
    W = W + new_connections;
    
    % start with a number of random connections to speed the
    % process
    if (synaptogen_round < SYNAPTOGEN_KICKSTART_ROUNDS) continue; end;
    
    % if not enough connections yet to possibly fire,
    % just do more synaptogenesis
    weight_sums = sum(W);
    if (max(weight_sums) < THRESH) continue; end
    
    % -------------------------------
    % train the network with the current set of connections
    % -------------------------------
    
    for iteration=1:INTER_SYNAPTOGEN_ITERATIONS
        
        % train on each input pattern, taken in random order
        for i=randperm(PATTERNS_COUNT)
            input_pattern = input(:,i);
            
            % calculate excitation
            W = W .* (W >= W_CUTOFF);
            excitation(:) = input_pattern' * W;

            % calculate and record activation
            Z(:) = excitation > THRESH;
            avgActivation = (1-ACTIVITY_RATE_CONST)*avgActivation ...
                + ACTIVITY_RATE_CONST*Z;
            
            % modify synaptic weights
            X = inputPatternMats{i};
            Y(:) = repRowVectFast(excitation, FEATURES_COUNT);
            dW(:) = W_RATE_CONST .* Y .* (X - W - E_X);
            W = W + dW;
            
        end % run through input patterns
    end % inter-synaptogenesis training cycles
    
    % -------------------------------
    % logging
    % -------------------------------
    
    W_above_cutoff = (W >= W_CUTOFF);
%     W_above_cutoff = W;
    individual_synapse_counts = sum(W_above_cutoff);
    
    synapses_count = sum(individual_synapse_counts);
    synapses_count = full(synapses_count);  % un-sparse so normal int
    synaptogen_count = sum(sum(new_connections > 0) );
    synaptoshed_count = synapses_count - old_synapses_count;
    synapseCountChange = (synapses_count - old_synapses_count);
    old_synapses_count = synapses_count;
    
    % record data in our logs object
    if (LOG)
        L.activation_log{end+1}             = mean(avgActivation);
        L.synaptogen_log{end+1}             = synaptogen_count;
        L.synapses_log{end+1}               = synapses_count;
        L.synapses_individual_log{end+1}    = individual_synapse_counts';
        L.receps_individual_log{end+1}      = recep_vect';
        L.synaptoshed_log{end+1}            = synaptoshed_count;
    end
    
    % every tenth round of synaptogenesis, log some information
    % if desired
    if (VERBOSE || LOG)
        isTenthRound = (mod(synaptogen_round, 10) == 0);
        if ( isTenthRound )
            log('\nRounds synaptogen: %d\t', synaptogen_round)
            log('Synapses: %d\t', synapses_count)
            log('Change in synapse count: %.3f\n', synapseCountChange)
            log('Neuron Activities:\n')
            log('%.3f ', avgActivation)
            log('\n')
        end
        
%         if synaptogen_round > 20 return; end    %TODO remove
    end

    % -------------------------------
    % synaptic shedding
    % -------------------------------
    
    % synapses now shed at each iteration for speed
%     W = W .* W_above_cutoff;
    
    % -------------------------------
    % termination test
    % -------------------------------
    
    
    delta_synapses = (1-DELTA_SYNAPSES_RATE_CONST) * delta_synapses ...
        + DELTA_SYNAPSES_RATE_CONST * synapseCountChange;
    
    % chekc if the network is stable
    if (synapseCountChange == 0)
        stableRounds = stableRounds + 1;
    else 
       stableRounds = 0; 
    end
    
    % check if the network has been stable for long enough
    networkFinished = (stableRounds >= minRoundsOfStability);
    
%     if (ENSURE_STABILITY)
%         networkFinished = all(avgActivation > MIN_FIRING_RATE) && ...
%             delta_synapses < DELTA_SYNAPSES_FINISH_THRESH;
%     else
%         networkFinished = all(avgActivation > MIN_FIRING_RATE);
%     end
    
    if (synaptogen_round >= SYNAPTOGEN_ROUNDS_LIMIT)
        networkFinished = 1;
    end
end % network construction

fprintf('\nNetwork Finished: rounds synaptogen, synapses: %d, %d\n', ...
    synaptogen_round, synapses_count);
end