function [avgMetrics metrics combinedMetrics] = simulate(InputSets, TestSets, NetParams, runs, loadData)
% [avgMetrics runMetrics combinedMetrics] = simulate(Input, Parameters, runs, load)
%
% Trains a single-layer, feed-forward network and returns
% various statistics regarding the results.
%
% PARAMETERS:
%   InputSets - a cell array of InputSet objects detailing the inputs
%       on which to train the network
%   TestSets - a cell array of InputSet objects detailing the inputs
%       on which to test the network. There must be one test set for
%       each input set.
%   NetParams - a NetworkParams object defining how the network
%       is to be constructed
%   runs - the number of networks that should be constructed, and
%       thus how many results should be averaged together for
%       greater accuracy.
%   loadData (=0) - whether to attempt to load saved data from a previous
%       call to this function. Default is no, meaning that new data
%       will be generated. It is strongly suggested that one not
%       generate new data unnecessarily, since this is typically
%       very time consuming.
%
% RETURN VALUES:
%   avgMetrics - a cell array each of whose entries is a NetworkMetrics 
%       object detailing numerous stastistics about the networks
%       constructed for each input set. All values are the averages
%       across the specified number of runs.
%   runMetrics - a cell array containing the NetworkMetrics for each
%       individual network constructed. Each row corresponds to one input
%       set, and each column corresponds to one run number.
%   combinedMetrics - a cell array each of whose fields is a
%       NetworkMetrics object containing the concatenation of the values
%       for that field for all individual runMetrics.
%
% See also ANALYZENETWORK, CREATENETWORK

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

SetDefaultValue(4, 'loadData', 0);

% -------------------------------------
% process and derive needed info from input args
% -------------------------------------

numInputs = length(InputSets);
metrics = cell(numInputs, runs);

% -------------------------------------
% execute the specified number of runs for each input
% -------------------------------------

for inputNum=1:numInputs
    inputSet = InputSets{inputNum};
    testSet = TestSets{inputNum}; 

    % construct / load multiple networks for each input
    % set so we can compute averages and thus reduce error
    for run=1:runs
        
        % determine file name to save weights as
        fileName = generateFileName(inputSet.name, NetParams, run);
        resultsFileName = [fileName,'_w.mat'];
        
        % either determine weights by constructing a new
        % network, or by just loading the weights from a
        % previous execution
        if (loadData)
            file = load(resultsFileName);
            W = file.W;
            L = file.L;
        else
            [W, L] = createNetwork(inputSet, NetParams);
            save(resultsFileName, 'W', 'L');
        end
        
        % store the network metrics for this network so we can
        % average them together at the end
        metrics{inputNum,run} = analyzeNetwork(testSet, W, NetParams, run);
        metrics{inputNum,run}.netConstructionLogs = L;
    end
end

% -------------------------------------
% average the runs together and display the results
% -------------------------------------

avgMetrics = cell(numInputs,1);
for i=1:numInputs
    avgMetrics{i,1} = averageStructs(metrics{i,:});
end

% -------------------------------------
% combine the runs together and display the results
% -------------------------------------

combinedMetrics = cell(numInputs,1);
for i=1:numInputs
    combinedMetrics{i,1} = catStructs(metrics{i,:});
end

end