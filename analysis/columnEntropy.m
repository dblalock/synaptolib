function [entropy uniqColCounts uniqCols] = columnEntropy(A)
% [entropy uniqColCounts uniqCols] = columnEntropy(A)
%
% Computes the Shannon entropy of a matrix A, where each column of A
% is treated as a symbol. That is, a matrix of n columns will
% be treated as a message of n symbols, and identical columns will
% be treated as identical symbols.
%
% This function is basically for computing the entropy of input and output
% firing patterns in neural networks.

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

[~, numCols] = size(A);

% if A contains no patterns, no entropy; just return
if (numCols == 0)
    entropy = 0;
    uniqColCounts = 0;
    uniqCols = [];
    return
end

% Our technique for finding the counts is to sort them all and then see
% how many in a row are the same.
sortedA = sortrows(A')';
uniqColCounts = [];
uniqCols = [];
lastVect = not(sortedA(:,1)); % can't match 1st col or 1st symbol count will be off by 1
for i=1:numCols
    if (sortedA(:,i) == lastVect)
        uniqColCounts(end) = uniqColCounts(end) + 1;
    else
        uniqColCounts = [uniqColCounts 1];
        uniqCols = [uniqCols sortedA(:,i)];
    end
    lastVect = sortedA(:,i);
end

% use our entropy function to calculate output entropy
entropy = shannonEntropy(uniqColCounts);

end