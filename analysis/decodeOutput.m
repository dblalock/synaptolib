function [classifications minDists errs classificationInfo] = ...
    decodeOutput(codewords, catStartIndices)
% [classifications minDists errs classificationInfo] = decodeOutput(output, catStartIndices)
%
% Given the output firing of a network for a set of patterns and the indices
% where categories start in the input pattern set, returns the classication 
% of each pattern at the output.
%
% PARAMETERS:
%   codewords - a logical matrix whose rows are the firing across output neurons
%       for a given pattern, and whose columns are (consequently) the
%       the firing across input patterns for each output neuron
%   catStartIndices - the first indices within the input set at which
%       patterns of a given category occur (patterns within the same
%       category are assumed to be grouped together)
%
% RETURN VALUES:
%   classifications - a column vector containing the classification of 
%       each input pattern. This is expressed as a number k within the 
%       range 1:(number of categories), as defined by catStartIndices
%   minDists - a column vector containing the distance between the
%       output codeword for each input pattern and the nearest centroid
%   errs - a logical column vector in which a 1 at index i means that the
%       the classification of pattern i was incorrect
%   classificationInfo - the mutual information between the input 
%       classifications and the output classifications

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% find the centroids of the output codes produced by inputs of each
% category
s = patternSetStats(codewords, catStartIndices);  % this just does everything
centroids = s.centroids;

% find which centroid each output code is closest to in euclidean space; 
% the distance matrix will have one row for each centroid, so we can 
% just find the minimum value in each column to see which centroid is 
% closest. By storing the row idx at which this occurs, we also get the
% classification.
dists = euclideanDists(centroids, codewords);
[minDists, classifications] = min(dists);

% convert to column vectors
minDists = minDists(:);
classifications = classifications(:);

% identify classification errors
[~,numPatterns] = size(codewords);
rightAnswers = generateCategoryLabels(numPatterns, catStartIndices);
errs = (rightAnswers - classifications) ~= 0;

% calculate mutual information between input classifications
% and output classifications
classificationInfo = mutualInfo(rightAnswers', classifications');


% account for codewords that are all 0s (no firing)
%   actually don't; that doesn't really belong in this function
% zeroIndices = find(sum(output, 2) == 0);

end