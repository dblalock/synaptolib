function infos = representationalInfos(inputPatterns, outputPatterns, catStartIndices)
% bitsForCategories = representationalInfos(input, output, catStartIndices)
%
% Returns the mutual information between input and output patterns of
% each category
%
% PARAMETERS:
%   inputPatterns - a matrix whose columns are the input codewords
%   outputPatterns - a matrix whose columns are the output codewords, with
%       each corresponding to the input pattern of the same index
%   catStartIndices - the first indices within the input set at which
%       patterns of a given category occur (patterns within the same
%       category are assumed to be grouped together)
%
% RETURN VALUES:
%   bitsForCategories - a row vector each of whose ith entry is the mutual
%       information between the input patterns of category i and their
%       corresponding output patterns

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

numCategs = length(catStartIndices);

infos = zeros(1, numCategs);

% separate input and output codewords for each category
catInputs = splitMat(inputPatterns, catStartIndices);
catOutputs = splitMat(outputPatterns, catStartIndices);

% calculate mutual info between input and output patterns for each category
for i=1:numCategs
    infos(i) = mutualInfo(catInputs{i}, catOutputs{i});
end

end