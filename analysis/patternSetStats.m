function [s] = patternSetStats(patternMat, catStartIndices)
% S = patternSetStats(input, catStartIndices)
%   Computes various statistics for the pattern set specified
%   
%   PARAMETERS:
%   patternMat - a matrix whose columns are patterns. The output of this
%       function has only been validated with boolean-valued patterns, but
%       there's no reason to believe it shouldn't work for others.
%   catStartIndices - the columns in which each category begins;
%       (patterns are assumed to be grouped by category).
%
%   RETURN VALUES:
%   S - a struct containing the following fields:
%       numCategories - the number of categories in the input
%       centroids - the central point in euclidean space of each category's
%           patterns
%       centroidDists - the average distance from a pattern to the centroid,
%           computed for each category
%       centroidDistVars - the variance in these distances across patterns,
%           computed for each category
%       meanCentroidDist - the average distance to the centroid across all
%           categories
%       centroidCos - the average cosine distance between a pattern and the
%           the centroid, computed for each category
%       centroidCosVars - the variance in these distances across patterns,
%           computed for each category
%       meanCentroidCos - the average cosine distance to the centroid across
%           all categories
%       patternDists - the average Euclidean distance between a pattern and
%           each other pattern in its category, computed for each category
%       patternDistsVars - the variance in these distances across patterns,
%           computed for each category
%       patternCos - the average cosine distance between a pattern and each
%           other pattern in its category, computed for each category
%       patternCosVars - the variance in these distances across patterns,
%           computed for each category
%       meanPatternCos - the average cosine distance to the other patterns
%           in the same category across all categories
%       patternHams - the average hamming distance between a pattern and
%           each other pattern in its category, computed for each category
%       patternHamVars - the variance in these distances across patterns,
%           computed for each category
%       entropy - the entropy of the full pattern set, where distinct
%           patterns are taken as distinct symbols
%       entropies - the entropies of each category, where distinct patterns
%           are taken as distinct symbols
%       statDep - the statistical dependence of the full pattern matrix
%       statDeps - the statistical dependences of each category
%       meanInterCentroidDist - the average euclidean distance between
%           centroids
%       varInterCentroidDist - the variance in euclidean distance between
%           centroids
%       meanInterCentroidCos - the average cosine distance between
%           centroids
%       varInterCentroidCos - the variance in cosine distance between
%           centroids

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%   NOTE: it would probably be better to compute {mean, variance} of
%   {euclidian, cosine, hamming} distance to {centroid, all other
%   patterns} for {each category, overall}, but presently we're just doing a
%   subset of this.

% compute columns where categories stop (since we know where they start)
[features, numPatternsTotal] = size(patternMat);
catStopIndices = stopIndicesFromStartIndices(catStartIndices, numPatternsTotal);

% compute number of categories
numCategs = length(catStartIndices);
s.numCategories = numCategs;

% compute metrics for pattern set as a whole
s.entropy           = columnEntropy(patternMat);
s.statDep           = statisticalDependence(patternMat);

% initialize vars for category-specific stats
s.centroids         = zeros(features,numCategs);
s.centroidDists     = zeros(1, numCategs);
s.centroidDistVars  = zeros(1, numCategs);

s.centroidCos       = zeros(1, numCategs);
s.centroidCosVars   = zeros(1, numCategs);

s.patternDists      = zeros(1, numCategs);
s.patternDistVars   = zeros(1, numCategs);

s.patternCos        = zeros(1, numCategs);
s.patternCosVars    = zeros(1, numCategs);

s.patternHams       = zeros(1, numCategs);
s.patternHamVars    = zeros(1, numCategs);

s.entropies         = zeros(1, numCategs);
s.statDeps          = zeros(1, numCategs);

% compute statistics for each category
for c = 1:numCategs

    % extract patterns contained within the current category
    r1 = catStartIndices(c);
    r2 = catStopIndices(c);
    numCategPatterns = r2 - r1 + 1;
    categPatterns = patternMat(:,r1:r2);

    % find centroids for each category
    centroid = mean(categPatterns, 2);
    s.centroids(:,c) = centroid;

    % find euclidean distances to centroid
    distsToCentroids = euclideanDists(centroid, categPatterns);
    s.centroidDists(c) = mean(distsToCentroids);
    s.centroidDistVars(c) = var(distsToCentroids);
    
    % find cosines to centroid
    centroidCosines = cosineComp(centroid, categPatterns);
    s.centroidCos(c) = mean(centroidCosines);
    s.centroidCosVars(c) = var(centroidCosines);

    % find euclidean distances between all pairs of patterns within category
    patternDists = euclideanDists(categPatterns, categPatterns);
    s.patternDists(c) = mean(mean(patternDists));
    s.patternDistVars(c) = var(mean(patternDists));
    
    % find cosines between all pairs of patterns within category
    patternCosines = cosineComp(categPatterns, categPatterns);
    s.patternCos(c) = mean(mean(patternCosines));
    s.patternCosVars(c) = var(mean(patternCosines));
    
    % find hamming distances between all pairs of patterns within category
    % TODO this should be its own function
    hDists = zeros(numCategPatterns, numCategPatterns);
    for i=1:numCategPatterns
        for j=1:numCategPatterns
            hDists(i,j) = sum(categPatterns(:,i) ~= categPatterns(:,j) );
        end
    end
    s.patternHams(c) = mean(mean(hDists));
    s.patternHamVars(c) = var(hDists(:));
    
    % find entropy within each category
    s.entropies(c) = columnEntropy(categPatterns);
    
    % find statistical dependence within each category
    s.statDeps(c) = statisticalDependence(categPatterns);
end

% compute mean values and variances across whole pattern set
s.meanCentroidDist = mean(s.centroidDists);
s.meanCentroidCos = mean(s.centroidCos);
s.meanPatternCos = mean(s.patternCos);

interCentroidDists = euclideanDists(s.centroids, s.centroids);
interCentroidCos = cosineComp(s.centroids, s.centroids);

s.meanInterCentroidDist = mean(interCentroidDists(:));
s.varInterCentroidDist = var(interCentroidDists(:));
s.meanInterCentroidCos = mean(interCentroidCos(:));
s.varInterCentroidCos = var(interCentroidCos(:));

% structdisp(s) % prints out the struct and the calls disp() on all its values

end
