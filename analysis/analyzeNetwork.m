function m = analyzeNetwork(Input, W, P, run)
% networkMetrics = analyzeNetwork(Input, W, P, run)
%
% 	Analyze a network with a given weight matrix, firing threshold, minimum
% 	firing rate (receptivity cutoff), input set, and (for labeling figures),
% 	a given input set name.
%
% PARAMETERS:
% 	Input 		- the InputSet object on which the network was trained
% 	W 			- a featuresCount x neuronCount matrix of synaptic weights
%   p           - a NetworkParams object describing the traits of the
%                   network
%   run         - if multiple runs of the simulation with the same set of
%                   parameters are being used, this can be set to
%                   distinguish them in the names of the saved analysis files.
%
% RETURN VALUES:
% 	networkMetrics - a struct consisting of:
%		TBD
%
% TODO write about what this does once we decide + code it

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

SetDefaultValue(4, 'run', -1)

% ================================================================
% CONSTANTS
% ================================================================

% -------------------------------
% constants altering code execution
% -------------------------------

% don't calculate the error rates as neurons as removed, since this is slow
SKIP_ERROR_RATES = 1;

% -------------------------------
% constants derived from arguments
% -------------------------------

% extract input fields from data structure
input = Input.inputPatterns;
inputName = Input.name;
catStartIdxs = Input.catStartIdxs;

% extract threshold and min firing rate from data structure
THRESH = P.threshold;
MIN_FIRING = P.minFiringRate;

% -------------------------------
% constants pertaining to energy costs
% -------------------------------
COST_SYNAPSE_USED = 1;
COST_SYNAPSE_UNUSED = 1/15;
COST_FIRE_0 = .75;
COST_FIRE_1 = 3.75;

% -------------------------------
% constants pertaining to synapse parameters
% -------------------------------

NEURON_ACT_DISCARD_THRESH = 0 + eps;	% 0 not quite high enough
NEURON_DISCARD_FRACTIONS = 0:.025:.95;

% ================================================================
% HELPER FUNCTIONS
% ================================================================

% initialize our log file and define a function to write to it
% (as well as print to the console). The file's name is generated
% based on the parameters of the network

networkName = generateFileName(Input.name, P, run);
logFileName = [networkName,'_analysis.txt'];
logFile = fopen(logFileName, 'w');
if (logFile == -1) 
    error(['failed to open log_file: ',logFileName]);
end

function log(varargin)
    fprintf(varargin{:});
    if (logFile ~= -1)
        fprintf(logFile, varargin{:});
    end
end

% ================================================================
% MAIN
% ================================================================

% display what we're analyzing
log('Analyzing %s: thresh = %.2f, minFiring = %.2f, run%d\n', ...
    inputName, THRESH, MIN_FIRING, run);

% initialize return value
m = NetworkMetrics();

% -------------------------------
% Remove neurons that don't fire enough
% -------------------------------

% output dimensions: patternsCount x neuronCount
excitation = (input' * W);
output = excitation > THRESH;

[~, m.totalNeurons] = size(output);

% remove neurons that don't fire enough
nonzeroNeuronIdxs = find( mean(output) >= NEURON_ACT_DISCARD_THRESH );
output = output(:, nonzeroNeuronIdxs);
W = W(:, nonzeroNeuronIdxs);
m.weights = W;
[~, m.nonzeroNeurons] = size(output);

% if none of the neurons fired, just display a warning and return
if ( isempty(output) || isempty(W) )
	disp('WARNING: output or weight matrix empty.')
	return
end

% -------------------------------
% Calculate basic statistics
% -------------------------------

% get basic info about resulting network so subsequent code will work
[~,m.numInputPatterns] = size(Input.inputPatterns);
m.excitation = excitation;
m.output = output;
m.catStartIdxs = catStartIdxs;
[featuresCount, neuronCount] = size(W);
[~, patternsCount] = size(input);
categoriesCount = length(catStartIdxs);

% input and output set statistics
m.inputSetStats = patternSetStats(input, catStartIdxs);
m.outputSetStats = patternSetStats(output', catStartIdxs);

numCategories = length(catStartIdxs);
featureStartIdxs = 1:(featuresCount/numCategories):featuresCount;
    % TODO don't assume equiprobable categories!! XXX
m.eigenVals = eigenValsForCategories(input, featureStartIdxs);

% activity
m.activs = mean(output);
m.minAct  = min(m.activs);
m.maxAct  = max(m.activs);
m.stdAct  = std(m.activs);
m.meanAct = mean(m.activs);

% synapses and synaptic weights
synapsesMat = W .* (W > 0);
m.synapseCounts = sum(synapsesMat > 0, 1);
m.weightSums = sum(W, 1);

% -------------------------------
% Calculate costs and efficiency
% -------------------------------

% energy cost
synapses_total = sum(sum( W > 0 ) );
synapses_used  = sum(sum( input ) );
m.synapseCosts = synapses_used * (COST_SYNAPSE_USED - COST_SYNAPSE_UNUSED) ...
    + COST_SYNAPSE_UNUSED * synapses_total;
% m.synapseCosts = round(m.synapseCosts); % over 1000, so might as well round to make it pretty

firing_ones = sum(sum( output ));
m.firingCosts = firing_ones * (COST_FIRE_1 - COST_FIRE_0) ...
	+ COST_FIRE_0 * neuronCount;

totalCost = m.synapseCosts + m.firingCosts;
m.meanCost = totalCost / patternsCount;
m.meanCostPerNeuron = m.meanCost / neuronCount;

% output entropy
[output_entropy symbol_counts] = columnEntropy(output');

% number of codewords
m.codeWordCounts = sort(symbol_counts, 'descend');

% information and compression
m.statDep = statisticalDependence(output);
m.outputInfo = output_entropy;
m.mutualInfo = mutualInfo(input, output');
m.bitsPerJ = m.mutualInfo / m.meanCost;


% -------------------------------
% Determine resource allocations
% -------------------------------

% resource allocation metrics
m.neuronAllocs = neuronAllocations(output, catStartIdxs);
m.repCapacities = representationalCapacities(output, catStartIdxs);
m.repInfos = representationalInfos(input, output', catStartIdxs);

% figure out synapse counts + weights for each category
firing = firingForCategories(output, catStartIdxs);
neuronsCodingCategories = firing > 0;
m.synapsesForCategories = zeros(categoriesCount, 1);
m.weightSumsForCategories = zeros(categoriesCount, 1);
for i=1:categoriesCount
	codingNeuronIdxs = find(neuronsCodingCategories(i,:));
	
	% if no neurons coded for this category, we can just leave the values as 0
	if ( isempty(codingNeuronIdxs) ) 
        continue;
    end

	% otherwise, find avg number of synapses and avg sum of synaptic weights
	m.synapsesForCategories(i) = sum(sum( synapsesMat(:,codingNeuronIdxs) )) ...
		 / length(codingNeuronIdxs);
	m.weightSumsForCategories(i) = sum(sum( W(:, codingNeuronIdxs) )) ...
		/ length(codingNeuronIdxs);
end

% -------------------------------
% Determine classifications / error rates
% -------------------------------

% classifications
[~, ~, errs, m.classificationInfo] = decodeOutput(output', catStartIdxs);
m.numClassifyErrs = sum(errs);
m.classificationBitsPerJ = m.classificationInfo / m.meanCost;

% error rates when discarding random neurons
if ~SKIP_ERROR_RATES
    m.discardErrRates = errorsVsDiscardFractions(output', catStartIdxs, ...
        NEURON_DISCARD_FRACTIONS);
end
m.discardFractions = NEURON_DISCARD_FRACTIONS;


end