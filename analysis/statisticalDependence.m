function dep = statisticalDependence(X)
% dep = statisticalDependence(X)
%
% Given a matrix with columns representing distinct symbols, 
% computes Sum(H(Xi)) - H(X)

[~, xCols] = size(X);

% get the entropy for the whole set of codewords (columns)
H_x = columnEntropy(X);

% now find the entropies for each feature of X (rows)
sums = sum(X, 2);
probabilities = sums / xCols;
individualEntropies = bernoulliEntropy(probabilities);
sum_H_Xi = sum(individualEntropies);

% statistical dependence is the difference between these two values
dep = sum_H_Xi - H_x;

end