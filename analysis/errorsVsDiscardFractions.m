function errRates = errorsVsDiscardFractions(codewords, catStartIdxs, discardFractions, numTrials)
% errRates = errorsVsDiscardFractions(codewords, catStartIdxs, discardFractions, numTrials)
%
% Computes the error rate in decoding for each of the specified
% discardFractions
%
% PARAMETERS:
%   codewords - a matrix whose columns are the output codewords
%   catStartIdxs - the first indices within the output matrix at which
%       outputs for patterns of a given category occur (patterns within 
%       the same category are assumed to be grouped together)
%   discardFractions - a vector of fractions of the output neurons to 
%       discard. Each fraction is tested and the corresponding error rate
%       is returned.
%   numTrials (=100) - the number of times to test each discard fraction
%       in order to produce a more accurate error rate.
%
% RETURN VALUES:
%   errRates - a row vector containing one entry for each discardFraction
%       given. This is a the total figure across the entire network,
%       computed numTrials times and averaged together.

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% Given a set of output, codewords, the indices at which categories begin,
% the fraction of neurons to 
SetDefaultValue(4, 'numTrials', 200);

%TODO describe what counts as an "error" better
%   at present, it's an error if there's at least one category
%   with no output codeword in the decimated codeword matrix

numCats = length(catStartIdxs);
numDiscardFracs = length(discardFractions);
errRatesBuff = zeros(numTrials, numDiscardFracs);
classifications = decodeOutput(codewords, catStartIdxs);
for i=1:numDiscardFracs
    frac = discardFractions(i);
    for trial = 1:numTrials

        [~, keptIdxs] = removeRandomNeurons(codewords, frac);
        keptClasses = classifications(keptIdxs);
        
        % if one of each category here, we're good
        keptCats = unique(keptClasses);
        succeeded = (length(keptCats) == numCats);

        errRatesBuff(trial, i) = ~succeeded;
    end
end

errRates = mean(errRatesBuff, 1);

end