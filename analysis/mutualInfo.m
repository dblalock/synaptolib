function info = mutualInfo(X, Y)
% info = mutualInfo(output, categStartIndices)
%
% Returns the mutual information of X and Y, where columns of these
% matrices are treated as codewords.
% 
% PARAMETERS:
%   X - a matrix whose columns are codewords (ie, symbols).
%   Y - a matrix whose columns correspond to the system output 
%       for each input column
%   catStartIndices - the first indices within the input set at which
%       patterns of a given category occur (patterns within the same
%       category are assumed to be grouped together)
%
% RETURN VALUES
%   info - the mutual information of X and Y

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

% columns representing symbols are hard to work with. Let's assign
% each an integer instead
x = cols2symbols(X);
y = cols2symbols(Y);

% create a matrix containing the number of times input pattern i is
% paired with output pattern j. We can then divide by the total number
% of patterns to obtain the joint PMF.
numUniqInputs = length(unique(x));
numUniqOutputs = length(unique(y));
jointOccurences = zeros(numUniqInputs, numUniqOutputs);

for i=1:length(x)
    jointOccurences(x(i),y(i)) = jointOccurences(x(i),y(i)) + 1;
end

% find the joint and marginal PMFs
pXY = jointOccurences ./ sum(jointOccurences(:));
pX = sum(pXY,2);
pY = sum(pXY,1);

% plug these PMFs into the mutual information formula, replacing
% NaNs with 0 so we get the right answers.
info = pXY .* log2(pXY ./ (pX*pY) );
info(isnan(info)) = 0;
info = sum(info(:));

end