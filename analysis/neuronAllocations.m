function neuronsForCategories = neuronAllocations(output, categStartIndices)
% neuronsForCategories = neuronAllocations(output, categStartIndices)
%
% Given an output matrix and indices of the rows in which the output
% for each category starts, returns a column vector containing the number
% of neurons allocated to each category
%
% This is calculated as the sum across all neurons of the P(C=x|Yj=1) for
% each category
%
% Output is assumed to have a column for each neuron, 
% and a row for each input pattern

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

cat_count = length(categStartIndices);

% get the total firing for each neuron for each category
firing = firingForCategories(output, categStartIndices);

% we need to convert total firing for each category to a relative
% frequency, so we need the total firing for each neuron overall
totalFirings = sum(output);

% we now divide the sums for each neuron to get probabilities
firing = firing ./ ...
    repRowVectFast(totalFirings, cat_count);
firing(isnan(firing)) = 0;

% now sum the probabilities across all the neurons
neuronsForCategories = sum(firing, 2);

% this had better be a row vector of ones, and it is
% Note, however, that sometimes the result will just be very close
% to 1, so we comment the error part out for actual testing
% 
% And actually, a lot of the time there will be a neuron that doesn't
% fire at all, in which case we'll get a 0 and this won't work
% if (any(sum(firing) ~= 1) )
%    disp('WARNING: neuronAllocations: neuron probabilities dont sum to 1!')
%    theOutput = output
%    firingForCategs = firingForCategories(output, categStartIndices);
%    probabilities = sum(firing)
%    error('ERR: neuronAllocations: neuron probabilities dont sum to 1!')
% end

end