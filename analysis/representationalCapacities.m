function bitsForCategories = representationalCapacities(output, categStartIndices)
% bitsForCategories = representationalCapacities(output, categStartIndices)
%
% Given an output matrix and indices of the rows in which the output
% for each category starts, returns a column vector containing the number
% of bits allocated to representing each category
%
% This is calculated as the sum across all neurons of the H(Yj|C=x) for
% each category
%
% Output is assumed to have a column for each neuron, 
% and a row for each input pattern

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

cat_count = length(categStartIndices);
[pattern_count, neuron_count] = size(output);

% first, we need to know how much each neuron is firing for each category
firing = firingForCategories(output, categStartIndices);

% then, we need to know how much it *could* have fired for that category;
% this is just the number of patterns it saw in that category
nextCategStartIndices = categStartIndices(2:cat_count);
nextCategStartIndices = [nextCategStartIndices pattern_count+1];
category_sizes = nextCategStartIndices - categStartIndices;

% next, we need the probabilities with which it fires for each category
category_sizes = repColVectFast(category_sizes', neuron_count);
firingProbabilities = firing ./ category_sizes;

% finally, we need the entropies associated with each of these
% probabilities
entropies = bernoulliEntropy( firingProbabilities );
bitsForCategories = sum(entropies, 2);

end