function [] = plotReceptivity(min_firing, P)

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

yRange = 0:.001:1;

Cr = min_firing .^ P;

receptivity = (ones(1,length(yRange)) * Cr) ./ (Cr + yRange .^ P);

skeletonPlot(yRange, receptivity, ...
    ['Receptivity vs Average Activation, Firing Rate ', num2str(min_firing)], ...
    'Average Activation', 'Receptivity', 'k-');

setFigureFontSize(20);
verticalLine(min_firing, 1);

end