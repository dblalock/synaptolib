function spyInput(InputObj)
% spyInput(InputObj)
%
% Creates a spy diagram of the specified input set
%
% PARAMETERS:
%   InputObj - the InputSet object of which to make a spy diagram
%
% RETURN VALUES:
%   None
%
% See also spyMat, spy

spyMat(InputObj.inputPatterns, 'Pattern Index', 'Feature Index', InputObj.name);

end