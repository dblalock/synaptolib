function visualizeNetwork(M, networkName, dirName)
% visualizeNetwork(M, networkName, dirName)
%
% Create figures characterizing the network. At present, the set of figures
% generated can be set by changing flags near the top of the file
%
% POSSIBLE FIGURES:
%   COSINE_COMP_OUTPUT
%       creates and imagesc()'s a cosine similarity matrix between all
%       output codewords. Note that this is not meaningful when the
%       network metrics have been averaged across multiple networks.
%   ERR_RATES
%       creates a plot of fraction of neurons removed vs decoding errors
%   WEIGHTS
%       creates a color coded image of the weight matrix
%   EUC_DIST_OUTPUT_HIST
%       creates a histogram of the all-pairs euclidean distances between
%       output patterns
%   ACTIVITIES_HIST
%       creates a histogram of output neuron activity levels across
%       the network
%   EXCITE_HIST
%       creates a histogram of output neuron excitation levels across
%       the network
%   SYNAPSES_HIST
%       creates a histogram of output neuron synapse counts across the
%       network
%   WEIGHTS_HIST
%       creates a histogram of sum of output neuron synaptic weights
%       across the network
%   SYNAPSES_FOR_CATEGS
%       creates a bar graph of the average number of synapses for
%       neurons in each category
%   WEIGHTS_FOR_CATEGS
%       creates a bar graph of the average sum-of-synaptic weights for
%       neurons in each category
%   NEUR_ALLOCS_FOR_CATEGS
%       creates a bar graph of the total of number of neurons allocated
%       to coding for each category
%   REP_CAPS_FOR_CATEGS
%       creates a bar graph of the total representational capacity allocated
%       to each category
%   MUT_INFOS_FOR_CATEGS
%       creates a bar graph of the mutual information between input and 
%       output codewords of each category
%   NEUR_ALLOCS_SCATTER
%       creates a scatter plot of category relative frequency vs neuron
%       allocation for that category
%   REP_CAPS_SCATTER
%       creates a scatter plot of category relative frequency vs
%       representaional capacity for that category
%
% PARAMETERS:
%   M           - a NetworkMetrics object describing the network to
%                   visualize
%   networkName - a name for the network to be used appended to filenames
%   dirName (='figs/') - directory in which to save figures
%
% RETURN VALUES:
%   None
%
% TODOs (Coming soon!):
%   -Pass in an object describing which visualizations to create

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

SetDefaultValue(3,'dirName','figs/')

% -------------------------------
% constants pertaining to figure creation
% -------------------------------

COSINE_COMP_OUTPUT      = 0;
WEIGHTS                 = 0;
ERR_RATES               = 0;
EUC_DIST_OUTPUT_HIST    = 0;
ACTIVITIES_HIST         = 0;
EXCITE_HIST             = 0;
SYNAPSES_HIST           = 0;
WEIGHTS_HIST            = 0;
SYNAPSES_FOR_CATEGS     = 0;
WEIGHTS_FOR_CATEGS      = 0;
NEUR_ALLOCS_FOR_CATEGS  = 0;
REP_CAPS_FOR_CATEGS     = 0;
MUT_INFOS_FOR_CATEGS    = 0;
NEUR_ALLOCS_SCATTER     = 1;
REP_CAPS_SCATTER        = 0;
EIG_ALLOC_SCATTER       = 0;
SYNAPSES_INDIVIDUAL_LOG = 0;

% ================================================================
% ERROR TRAPPING
% ================================================================

if M.nonzeroNeurons == 0
   disp('WARNING: visualizeNetwork: no nonzero neurons; exiting')
   return
end

% ================================================================
% FIGURE GENERATION
% ================================================================

% ensure trailing slash in directory name
slashIdxs = strfind(dirName, '/');
if (isempty(slashIdxs) || (slashIdxs(end) ~= length(dirName) ) )
   dirName = [dirName,'/']; 
end

% ensure that the appropriate directory exists and that the output file 
% is named properly
saveDir = sprintf(dirName);           % TODO should be a parameter
nameSuffix = ['-',networkName];
ensureDirExists(saveDir);

% make a utility function to save figures in the right dir
saveFig = @(figName) saveNetsPlot([saveDir,figName,nameSuffix]);

if ACTIVITIES_HIST
    % histogram of mean activity for final run
    activityMeans = mean(M.output);
    edges = 0:.05:.5;
    N = histc(activityMeans,edges);
    bar(edges,N,'histc')
    labelFigure('Mean firing rate across neurons',...
        'Mean firing rate', 'Number of neurons');
    saveFig('actMeans')
end

% cosine comparison of output symbols
if COSINE_COMP_OUTPUT
    cosineComparison = cosineComp(M.output', M.output');
    imagesc(cosineComparison)
    labelFigure('Cosine Comparison', 'output symbol index',...
        'output symbol index');
    colorbar
    saveFig('cosineComparison')
end

if EUC_DIST_OUTPUT_HIST
    name = ['euc_dist',networkName];
    euclideanDistsHist(M.output', M.catStartIdxs, name, saveDir);
end

if WEIGHTS
    figure()
    imagesc(M.weights)
    labelFigure('Synaptic Weights', 'Neuron index',...
        'Feature Index');
    colorbar
    saveFig('weights')
end

% histogram of mean excitation for final run
if EXCITE_HIST
    excitationMeans = mean(M.excitation);
    hist(excitationMeans, 20)
    labelFigure('Mean excitation across neurons',...
        'Mean excitation', 'Number of neurons');
    saveFig('exciteMeans')
end

if ERR_RATES
    plot(M.discardFractions, M.discardErrRates);
    axis([0 1 0 1]);
    labelFigure('Error rate vs fraction of output neurons removed',...
        'Fraction of neurons removed', 'Error rate');
    saveFig('errRate')
end

% histogram of number of synapses for final run
if SYNAPSES_HIST
    hist( M.synapseCounts )
    labelFigure('Number of synapses across neurons',...
        'Number of synapses', 'Number of neurons');
    saveFig('synapseCounts')
end

% bar graph of avg number of synapses for each category
if SYNAPSES_FOR_CATEGS
    bar(M.synapsesForCategories)
    axis('tight')
    labelFigure('Average number of synapses for neurons coding each category',...
        'Category', 'Number of synapses');
    saveFig('synapsesForCategories')
end

% histogram of weights across all neurons after final run
if WEIGHTS_HIST
    weights = M.weightSums;
    hist(weights,20)
    labelFigure('Frequency of total weight values across neurons',...
        'Sum of synaptic weights', 'Number of neurons');
    saveFig('weightsSums')
end

% bar graph of weight sumsfor each category
if WEIGHTS_FOR_CATEGS
    bar(M.weightSumsForCategories)
    axis('tight')
    labelFigure('Average sum of synaptic weights for neurons coding each category',...
        'Category', 'Number of synapses');
    saveFig('weightSumsForCategories')
end

% bar graph of neurons allocated to each category
if NEUR_ALLOCS_FOR_CATEGS
    bar(M.neuronAllocs);
    axis('tight')
    labelFigure('Neurons allocated to each category', ...
        'Category', 'Neurons Allocated')
    saveFig('neurAllocs')
end

% bar graph of representational capacity allocated to each category
if REP_CAPS_FOR_CATEGS
    bar(M.repCapacities);
    axis('tight')
    labelFigure('Representational capacity allocated to each category', ...
        'Category', 'Representational capacity allocated (bits)')
    saveFig('repCaps')
end

% bar graph of representational capacity allocated to each category
if MUT_INFOS_FOR_CATEGS
    bar(M.repInfos);
    axis('tight')
    labelFigure('Mutual info between input and output for each category', ...
        'Category', 'Mutual Information (bits)')
    saveFig('repInfos')
end

% scatter plot of neurons allocated to each category vs
% category relative frequency
if NEUR_ALLOCS_SCATTER
    
    % generate scatter plot
    relFreqs = calcRelativeFrequencies(M.catStartIdxs, M.numInputPatterns);
    scatter(relFreqs,M.neuronAllocs);
    
    % find best fit line for the scatter plot
    linearFit = polyfit(relFreqs,M.neuronAllocs,1);
    lineVals = polyval(linearFit, relFreqs);
    
    % display line and make plot pretty
    hold on
    xlim([0 max(relFreqs+.02)]);
    plot(relFreqs, lineVals);
    labelFigure('Neurons Allocated to Categories of Different Relative Frequencies', ...
        'Category Relative Frequency', 'Neurons Allocated')
    saveFig('scatterNeurAllocs')
end

if REP_CAPS_SCATTER
    % generate scatter plot
    relFreqs = calcRelativeFrequencies(M.catStartIdxs, M.numInputPatterns);
    scatter(relFreqs,M.repCapacities);
    
    % find best fit line for the scatter plot
    linearFit = polyfit(relFreqs,M.repCapacities,1);
    lineVals = polyval(linearFit, relFreqs);
    
    % display line and make plot pretty
    hold on
    xlim([0 max(relFreqs+.02)]);
    plot(relFreqs, lineVals);
    labelFigure('Representational Capacity for Categories of Different Relative Frequencies', ...
        'Category Relative Frequency', 'Representational Capacity')
    saveFig('scatterRepCaps')
end

% scatter plot of neurons allocated to each category vs
% dominant eigenvalue of category covariance matrix
if EIG_ALLOC_SCATTER
    
    % generate scatter plot
%     M.eigenVals, M.neuronAllocs
%     size(M.eigenVals)
    x = M.eigenVals(:);
    y = M.neuronAllocs(:);
    scatter(x,y);
    
    % find best fit line for the scatter plot
    linearFit = polyfit(x,y,1);
    lineVals = polyval(linearFit, M.eigenVals);
    rMat = corrcoef([x y]);
    r = rMat(2);    % first entry is autocorrelation, which is 1
    fprintf('r, r^2: %.3f, %.3f\n', r, r*r);
    
    % display line and make plot pretty
    hold on
    xlim([0 max(M.eigenVals+.02)]);
    plot(M.eigenVals, lineVals);
    labelFigure('Neurons Allocated to Categories with different Eigenvalues', ...
        'Dominant Eigenvalue', 'Neurons Allocated')
    text(.1,max(M.neuronAllocs-1),sprintf('r = %.3f', r));
    saveFig('scatterEigenAllocs')
end

if SYNAPSES_INDIVIDUAL_LOG
    logs = M.netConstructionLogs;
    synapseCounts = cell2mat(logs.synapses_individual_log)';
    plot(synapseCounts, 'LineWidth',1.25)
    title('Number of synapses for each neuron across simulation')
    ylabel('Number of Synapses')
    saveFig('stability')
end

end