function saveNetsPlot(fileName)
% saveNetsPlot(name)
%
% Saves the current figure as a png with the specified name
% (no need to append .png) and then closes it. If you'd like 
% to save all figures in a custom directory, just prepend this
% directory to the filename in this function and everything 
% will get saved there from then on.
%
% This function also increases the font size of all text in 
% the figure to 16 for better readability.

if (isempty(strfind(fileName, '.png') ) )
   fileName = [fileName,'.png'];
end

setFigureFontSize(16)
saveas(gcf, fileName); 
close

end