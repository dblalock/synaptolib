function euclideanDistsHist(patternSet, catStartIdxs, patternSetName, dirName)
% euclideanDistsHist(patternSet, catStartIdxs, inputName, dirName)
%
% Plots histograms for the all-pairs Euclidean distances between patterns
% of each category
%
% PARAMETERS:
%   patternSet - a matrix whose columns are patterns
%   catStartIdxs - the columns in which each category begins; (patterns are 
%       assumed to be grouped by category)
%   patternSetName - a name for the pattern set, appended to filenames
%   dirName (='figs/') - directory in which to save figures
%
% RETURN VALUES:
%   None

SetDefaultValue(4,'dirName','figs/')

numCategs = length(catStartIdxs);
catPatterns = splitMat(patternSet, catStartIdxs);

% find 
distMats = cell(1, numCategs);
for i=1:numCategs
    patterns = catPatterns{i};
    distMats{i} = euclideanDists(patterns, patterns);
end

figNamePrefix = [dirName, patternSetName];
for i=1:numCategs
    dists = distMats{i};
    hist( dists(:) )
    labelFigure('Euclidean distances between patterns',...
        'Euclidean distance', 'Number of pattern pairs');
    figName = [figNamePrefix,'_cat',num2str(i)];
    saveNetsPlot(figName);
end

end