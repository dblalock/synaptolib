function spyMat(A, xStr, yStr, titleStr)
% spyMat(A, xStr, yStr, titleStr)
%
% Create a labeled spy diagram of a matrix
%
% PARAMETERS:
%   A - the matrix of which to make a spy diagram
%   xStr - a string label for the x axis
%   yStr - a string label for the y axis
%   titleStr - a string to serve as the title
%
% RETURN VALUES:
%   None
%
% See also SPY

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

figure();
spy(A);
xlabel(xStr)
ylabel(yStr)
title(titleStr)
setFigureFontSize(18)

end