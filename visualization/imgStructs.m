function imgStructs(StructsCell, fieldName, xVals, yVals)
% imgStructs(StructsCell, fieldName, xVals, yVals)
%
% Creates an image of the values of fieldName across the structs
% in StructsCell
%
% PARAMETERS:
%   StructsCell - a 2D cell array of structs (or instances of a class)
%   fieldName - the name of the field whose values should be depicted
%   xVals (optional) - the values to be used for the x axis of the 
%       image; i.e., what's varying across the columns of StructsCell
%   yVals (optional) - the values to be used for the y axis of the 
%       image; i.e., what's varying across the rows of StructsCell
% 
% RETURN VALUES:
%   None
%
% Note that this function will probably crash if the field values
% aren't all scalars, since the resulting matrix won't have sensible
% dimensionality

% SynaptoLib: computational modeling of biological neural networks
% Copyright (C) 2013  Davis Blalock
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

[numRows, numCols] = size(StructsCell);
SetDefaultValue(3, 'xVals', 1:numCols)
SetDefaultValue(4, 'yVals', 1:numRows)

% numStructs = numel(StructsCell);

mat = extractStructField(StructsCell, fieldName);

imagesc(xVals, yVals, mat)
colorbar

end
